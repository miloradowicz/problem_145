
#include <limits>
#include <vector>
#include <iostream>

const int INF = std::numeric_limits<int>::max();

int dfs_to_1(const std::vector<std::vector<int>> &graph, std::vector<bool> p, int n, int s = 0) {
  if (s == 1)
    return 0;
  if (n == 0)
    return -1;
  int longest = -1;
  p[s] = true;
  for (auto t : graph[s]) {
    if (!p[t]) {
      int l = dfs_to_1(graph, p, n - 1, t);
      if (l != -1 && longest < l + 1)
        longest = l + 1;
    }
  }
  return longest;
}

int main() {
  int N;
  std::vector<std::vector<int>> graph;

  std::cin >> N;
  graph.resize(N);
  char t;
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      std::cin >> t;
      if (t == '1')
        graph[i].push_back(j);
    }
  }

  int l = dfs_to_1(graph, std::vector<bool>(N, false), N - 1);
  if (l != -1) {
    std::cout << l;
  }
  else {
    std::cout << 0;
  }

  return 0;
}